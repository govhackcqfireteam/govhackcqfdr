﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace GovHackCQFireTeam
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapHttpRoute(
                name: "ActionApi1",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            routes.MapHttpRoute(
               name: "ActionApi2",
               routeTemplate: "api/{controller}/{action}/{id1}/{id2}/{id3}/{id4}"
               //defaults: new { id1 = RouteParameter.Optional }
           );

            routes.MapHttpRoute(
             name: "ActionApi3",
             routeTemplate: "api/{controller}/{action}/{geohash}/{state}/{locationname}"
             //defaults: new { id = RouteParameter.Optional }
         );
        }
    }
}
