﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GovHackCQFireTeam.Controllers
{
    public class BOMForecastController : ApiController
    {
        private static HttpClient _Client = new HttpClient();
        private static string _APIKey = "9J2JpfNYoV2p40n0PaOsC2j6OyKIse2Q4Rz2HUjp";

        // GET: BOMForecast
        [HttpGet]
        public List<Models.BOMForecast> LoadLocation(string id)
        {
            return LoadLocation(id, null, null, null);
        }

        // GET: BOMForecast
        [HttpGet]
        public List<Models.BOMForecast> LoadLocation(string id1, double? id2, double? id3, double? id4)
        {
            var locationname = id1.Replace(" ", "-").Trim().ToLower();
            double userwindreductionfactor = id2.HasValue ? id2.Value : 3;
            var userfuelload = id3.HasValue ? id3.Value : 10;
            var userslope = id4.HasValue ? id4.Value : 0;

            var results = new List<Models.BOMForecast>();

            var webClient = new WebClient();
            var url = "http://www.bom.gov.au/places/qld/{0}/forecast/detailed/";
            var path = string.Format(url, locationname);
            var page = "";
            try
            {
                page = webClient.DownloadString(path);
            }
            catch (WebException)
            {
            }
            if (!string.IsNullOrEmpty(page))
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(page);
                var days = doc.DocumentNode.SelectNodes("//div[contains(@class,'forecast-day')]");
                if (days != null)
                {
                    foreach (var day in days)
                    {
                        var date = new DateTime(Convert.ToInt32(day.Id.Substring(1, 4)), Convert.ToInt32(day.Id.Substring(6, 2)), Convert.ToInt32(day.Id.Substring(9, 2)));
                        var temperature = day.SelectSingleNode("table[contains(@summary,'3 Hourly Temperatures')]");
                        if (temperature != null)
                        {
                            var header = temperature.SelectSingleNode("thead");
                            var body = temperature.SelectSingleNode("tbody");
                            if (header != null && body != null)
                            {
                                var headercells = header.SelectSingleNode("tr").SelectNodes("th");
                                if (headercells.Count > 1)
                                {
                                    var dates = new List<DateTime>();
                                    for (int i = 1; i < headercells.Count; i++)
                                    {
                                        var time = Convert.ToDateTime(headercells[i].InnerText);
                                        var timestamp = date.AddHours(time.Hour).AddMinutes(time.Minute);
                                        dates.Add(timestamp);
                                        if (!results.Where(x => x.Timestamp == timestamp && x.Location == locationname).Any())
                                            results.Add(new Models.BOMForecast() { Location = locationname, Timestamp = timestamp, UserWindReductionFactor = userwindreductionfactor, UserFuelLoad = userfuelload, UserSlope = userslope });
                                    }

                                    var bodyrows = body.SelectNodes("tr");
                                    if (bodyrows.Count >= 1)
                                    {
                                        var bodycells = bodyrows[0].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            double value = 0;
                                            if (double.TryParse(bodycells[i].InnerText, out value))
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.TemperatureAirC = value;
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                    if (bodyrows.Count >= 3)
                                    {
                                        var bodycells = bodyrows[2].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            double value = 0;
                                            if (double.TryParse(bodycells[i].InnerText, out value))
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.TemperatureDewPointC = value;
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var humidity = day.SelectSingleNode("table[contains(@summary,'3 Hourly Humidity')]");
                        if (humidity != null)
                        {
                            var header = humidity.SelectSingleNode("thead");
                            var body = humidity.SelectSingleNode("tbody");
                            if (header != null && body != null)
                            {
                                var headercells = header.SelectSingleNode("tr").SelectNodes("th");
                                if (headercells.Count > 1)
                                {
                                    var dates = new List<DateTime>();
                                    for (int i = 1; i < headercells.Count; i++)
                                    {
                                        var time = Convert.ToDateTime(headercells[i].InnerText);
                                        var timestamp = date.AddHours(time.Hour).AddMinutes(time.Minute);
                                        dates.Add(timestamp);
                                        if (!results.Where(x => x.Timestamp == timestamp && x.Location == locationname).Any())
                                            results.Add(new Models.BOMForecast() { Location = locationname, Timestamp = timestamp, UserWindReductionFactor = userwindreductionfactor, UserFuelLoad = userfuelload, UserSlope = userslope });
                                    }

                                    var bodyrows = body.SelectNodes("tr");
                                    if (bodyrows.Count >= 1)
                                    {
                                        var bodycells = bodyrows[0].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            if (!string.IsNullOrEmpty(bodycells[i].InnerText) && bodycells[i].InnerText != "&ndash;")
                                            {
                                                if (bodycells[i].InnerHtml.Contains("<br>"))
                                                {
                                                    double value = 0;
                                                    if (double.TryParse(bodycells[i].InnerHtml.Substring(0, bodycells[i].InnerHtml.IndexOf("<br>")), out value))
                                                    {
                                                        var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                        if (existing != null)
                                                        {
                                                            existing.WindSpeedKmH = value;
                                                            existing.UpdatedAt = DateTime.Now;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (bodyrows.Count >= 2)
                                    {
                                        var bodycells = bodyrows[1].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            if (!string.IsNullOrEmpty(bodycells[i].InnerText) && bodycells[i].InnerText != "&ndash;")
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.WindDirection = bodycells[i].InnerText.Trim();
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                    if (bodyrows.Count >= 3)
                                    {
                                        var bodycells = bodyrows[2].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            double value = 0;
                                            if (double.TryParse(bodycells[i].InnerText, out value))
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.HumidityRelativePercent = value;
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                    if (bodyrows.Count >= 4)
                                    {
                                        var bodycells = bodyrows[3].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            double value = 0;
                                            if (double.TryParse(bodycells[i].InnerText, out value))
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == locationname).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.ForestFuelDrynessFactor = value;
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }




            return results;
        }


        // GET: BOMForecast
        [HttpGet]
        public async Task<List<Models.BOMForecast>> LoadGrid(string geohash, string state, string locationname)
        {
            locationname = locationname.Replace(" ", "-").Trim().ToLower();
            state = state.ToLower().Trim();
            //configure timezone based on state
            TimeZoneInfo timezone = null;
            switch (state)
            {
                case "qld":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                    break;
                case "nsw":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                    break;
                case "act":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                    break;
                case "vic":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                    break;
                case "tas":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("Tasmania Standard Time");
                    break;
                case "sa":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("Cen. Australia Standard Time");
                    break;
                case "wa":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                    break;
                case "nt":
                    timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                    break;
                default:
                    break;
            }

            if (_Client.DefaultRequestHeaders.Count() == 0)
            {
                _Client.DefaultRequestHeaders.Add("x-api-key", _APIKey);
            }

            var results = new List<Models.BOMForecast>();
            var requestUri = string.Format("https://api.cloud.bom.gov.au/forecasts/v1/grid/three-hourly/{0}/temperatures", geohash);

            var response = await _Client.GetAsync(requestUri);
            if (response.IsSuccessStatusCode)
            {
                var jsonstring = response.Content.ReadAsStringAsync();
                jsonstring.Wait();
                //deserialize json
                dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                var data = d.data;
                //populate results from deserialized json
                foreach (var item in data.attributes["air_temperature"]["forecast_data"])
                {
                    DateTime timestamp = item.time;
                    timestamp = TimeZoneInfo.ConvertTimeFromUtc(timestamp, timezone);
                    //timestamp = timestamp.ToLocalTime();
                    var entry = results.Where(x => x.Location == geohash && x.Timestamp == timestamp).FirstOrDefault();
                    if (entry == null)
                    {
                        entry = new Models.BOMForecast() { Location = geohash, Timestamp = timestamp };
                        results.Add(entry);
                    }
                    entry.TemperatureAirC = item.value;
                }
                foreach (var item in data.attributes["dew_point_temperature"]["forecast_data"])
                {
                    DateTime timestamp = item.time;
                    timestamp = TimeZoneInfo.ConvertTimeFromUtc(timestamp, timezone);
                    //timestamp = timestamp.ToLocalTime();
                    var entry = results.Where(x => x.Location == geohash && x.Timestamp == timestamp).FirstOrDefault();
                    if (entry == null)
                    {
                        entry = new Models.BOMForecast() { Location = geohash, Timestamp = timestamp };
                        results.Add(entry);
                    }
                    entry.TemperatureDewPointC = item.value;
                }
                foreach (var item in data.attributes["humidity"]["forecast_data"])
                {
                    DateTime timestamp = item.time;
                    timestamp = TimeZoneInfo.ConvertTimeFromUtc(timestamp, timezone);
                    //timestamp = timestamp.ToLocalTime();
                    var entry = results.Where(x => x.Location == geohash && x.Timestamp == timestamp).FirstOrDefault();
                    if (entry == null)
                    {
                        entry = new Models.BOMForecast() { Location = geohash, Timestamp = timestamp };
                        results.Add(entry);
                    }
                    entry.HumidityRelativePercent = item.value;
                }
            }




            requestUri = string.Format("https://api.cloud.bom.gov.au/forecasts/v1/grid/three-hourly/{0}/wind", geohash);
            response = await _Client.GetAsync(requestUri);
            if (response.IsSuccessStatusCode)
            {
                var jsonstring = response.Content.ReadAsStringAsync();
                jsonstring.Wait();
                //deserialize json
                dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                var data = d.data;
                //populate results from deserialized json
                foreach (var item in data.attributes["wind_speed_kph"]["forecast_data"])
                {
                    DateTime timestamp = item.time;
                    timestamp = TimeZoneInfo.ConvertTimeFromUtc(timestamp, timezone);
                    //timestamp = timestamp.ToLocalTime();
                    var entry = results.Where(x => x.Location == geohash && x.Timestamp == timestamp).FirstOrDefault();
                    if (entry == null)
                    {
                        entry = new Models.BOMForecast() { Location = geohash, Timestamp = timestamp };
                        results.Add(entry);
                    }
                    entry.WindSpeedKmH = item.value;
                }
                foreach (var item in data.attributes["wind_direction"]["forecast_data"])
                {
                    DateTime timestamp = item.time;
                    timestamp = TimeZoneInfo.ConvertTimeFromUtc(timestamp, timezone);
                    //timestamp = timestamp.ToLocalTime();
                    var entry = results.Where(x => x.Location == geohash && x.Timestamp == timestamp).FirstOrDefault();
                    if (entry == null)
                    {
                        entry = new Models.BOMForecast() { Location = geohash, Timestamp = timestamp };
                        results.Add(entry);
                    }
                    string[] cardinals = { "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N" };
                    try
                    {
                        entry.WindDirection = cardinals[(int)Math.Round(((double)item.value * 10 % 3600) / 225)];
                    }
                    catch(ArgumentException)
                    {
                    }                    
                }
            }


            //processing to scrape equivalent webpage if available based on location name and retrieve forest fuel dryness factor as unavailable via grid api
            var webClient = new WebClient();
            var url = "http://www.bom.gov.au/places/{0}/{1}/forecast/detailed/";
            var path = string.Format(url, state, locationname);
            var page = "";
            try
            {
                page = webClient.DownloadString(path);
            }
            catch (WebException)
            {
            }
            if (!string.IsNullOrEmpty(page))
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(page);
                var days = doc.DocumentNode.SelectNodes("//div[contains(@class,'forecast-day')]");
                if (days != null)
                {
                    foreach (var day in days)
                    {
                        var date = new DateTime(Convert.ToInt32(day.Id.Substring(1, 4)), Convert.ToInt32(day.Id.Substring(6, 2)), Convert.ToInt32(day.Id.Substring(9, 2)));
                        var humidity = day.SelectSingleNode("table[contains(@summary,'3 Hourly Humidity')]");
                        if (humidity != null)
                        {
                            var header = humidity.SelectSingleNode("thead");
                            var body = humidity.SelectSingleNode("tbody");
                            if (header != null && body != null)
                            {
                                var headercells = header.SelectSingleNode("tr").SelectNodes("th");
                                if (headercells.Count > 1)
                                {
                                    var dates = new List<DateTime>();
                                    for (int i = 1; i < headercells.Count; i++)
                                    {
                                        var time = Convert.ToDateTime(headercells[i].InnerText);
                                        var timestamp = date.AddHours(time.Hour).AddMinutes(time.Minute);
                                        //subtract hour to bring in line with other data
                                        //timestamp = timestamp.AddHours(-1);
                                        dates.Add(timestamp);
                                        if (!results.Where(x => x.Timestamp == timestamp && x.Location == geohash).Any())
                                            results.Add(new Models.BOMForecast() { Location = geohash, Timestamp = timestamp });
                                    }
                                    var bodyrows = body.SelectNodes("tr");
                                    if (bodyrows.Count >= 4)
                                    {
                                        var bodycells = bodyrows[3].SelectNodes("td");
                                        for (int i = 0; i < bodycells.Count; i++)
                                        {
                                            double value = 0;
                                            if (double.TryParse(bodycells[i].InnerText, out value))
                                            {
                                                var existing = results.Where(x => x.Timestamp == dates[i] && x.Location == geohash).FirstOrDefault();
                                                if (existing != null)
                                                {
                                                    existing.ForestFuelDrynessFactor = value;
                                                    existing.UpdatedAt = DateTime.Now;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //process and remove days with no FDI
            var daylist = new List<DateTime>();
            //get days
            for (int i = 0; i < results.Count; i++)
            {
                var day = results[i].Timestamp.Date;
                if (!daylist.Contains(day))
                {
                    daylist.Add(day);
                }
            }
            for (int i = 0; i < daylist.Count; i++)
            {
                //if day has no ffdi results then remove results
                if (!results.Where(x => x.Timestamp.Date == daylist[i] && x.FFDI.HasValue).Any())
                {
                    for (int j = results.Count - 1; j >= 0; j--)
                    {
                        if(results[j].Timestamp.Date == daylist[i])
                        {
                            results.RemoveAt(j);
                        }
                    }
                }
            }

            return results.OrderBy(x => x.Timestamp).ToList();
        }
    }
}