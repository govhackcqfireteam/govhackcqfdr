﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GovHackCQFireTeam.Controllers
{
    public class BOMLocationController : ApiController
    {
        private static HttpClient _Client = new HttpClient();
        private static string _APIKey = "9J2JpfNYoV2p40n0PaOsC2j6OyKIse2Q4Rz2HUjp";

        [HttpGet]
        public string Details(string id)
        {
            return id;
        }


        // GET: BOMLocation
        [System.Web.Http.HttpGet]
        public async Task<List<Models.BOMLocation>> Search(string id)
        {
            var searchstring = id;
            var results = new List<Models.BOMLocation>();
            var requestUri = string.Format("https://api.cloud.bom.gov.au/search/v1/locations/?q={0}", searchstring.Replace(" ", "%20"));
            if (_Client.DefaultRequestHeaders.Count() == 0)
            {
                _Client.DefaultRequestHeaders.Add("x-api-key", _APIKey);
            }
            var response = await _Client.GetAsync(requestUri);            
            if (response.IsSuccessStatusCode)
            {
                var jsonstring = response.Content.ReadAsStringAsync();
                jsonstring.Wait();
                //deserialize json
                dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                var data = d.data;
                //populate results from deserialized json
                foreach (var item in data)
                {
                    results.Add(new Models.BOMLocation()
                    {
                        id = item.id,
                        type = item.type,
                        latitude = item.attributes["latitude"],
                        longitude = item.attributes["longitude"],
                        name = item.attributes["name"],
                        state = item.attributes["state"],
                        postcode = item.attributes["postcode"],
                        geohash = item.attributes["geohash"]
                    });
                }
            }



            return results;
        }
    }
}