﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GovHackCQFireTeam.Controllers
{
    public static class Facebook
    {
        private const string GRAPH_URL = "https://graph.facebook.com";
        private static HttpClient _Client = new HttpClient();
        private static Dictionary<string, string> _UserIds;

        /// <summary>
        /// Get the FB User Id from an access token
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <returns>User Id String</returns>
        public static async Task<string> GetUserId(string user_access_token)
        {
            if (_UserIds == null)
                _UserIds = new Dictionary<string, string>();

            if (_UserIds.ContainsKey(user_access_token))
                //if dictionary already contains the access token, return the corresponding key
                return _UserIds[user_access_token];
            else
            {
                //otherwise call the FB Graph API to get the user details
                var requestUri = string.Format("{0}/me?access_token={1}", GRAPH_URL, user_access_token);
                var response = await _Client.GetAsync(requestUri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json, then save and return the id
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                    string userid = d.id;
                    _UserIds.Add(user_access_token, userid);
                    return _UserIds[user_access_token];

                }
            }
            return null;
        }
        /// <summary>
        /// Retrieve the accounts associated with a user
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <returns>List of Facebook Account Details</returns>
        public static async Task<List<Models.FacebookAccount>> LoadAccounts(string user_access_token)
        {
            var results = new List<Models.FacebookAccount>();
            if (!string.IsNullOrWhiteSpace(user_access_token))
            {
                //build request uri 
                var requestUri = string.Format("{0}/me/accounts?access_token={1}", GRAPH_URL, user_access_token);
                //http get
                var response = await _Client.GetAsync(requestUri);
                //if successful response, proceed
                if (response.IsSuccessStatusCode)
                {
                    //read json string
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                    var data = d.data;
                    //populate results from deserialized json
                    foreach (var item in data)
                    {
                        results.Add(new Models.FacebookAccount()
                        {
                            Id = item.id,
                            AccessToken = item.access_token,
                            Name = item.name
                        });
                    }
                }
            }
            return results;
        }
        /// <summary>
        /// Posts a message to Facebook
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <param name="pageid">Page Identifier</param>
        /// <param name="message">Post Message</param>
        /// <param name="link">Link to Include in Post</param>
        /// <param name="privacy">Privacy Option for Post</param>
        /// <param name="scheduledtime">Scheduled Time for Post</param>
        /// <param name="photoids">List of identifiers of already posted, unpublished photos for </param>
        /// <returns>Post Id string</returns>
        public static async Task<string> PostMessage(string user_access_token, string pageid, string message, string link, string privacy, DateTime? scheduledtime, List<string> photoids)
        {
            string postid = null;
            if (!string.IsNullOrWhiteSpace(user_access_token) && !string.IsNullOrWhiteSpace(message))
            {
                //build request uri based on whether the post is to a page or a user
                var requestUri = string.Format("{0}/me/feed?access_token={1}", GRAPH_URL, user_access_token);
                if (!string.IsNullOrWhiteSpace(pageid))
                {
                    var accounts = await LoadAccounts(user_access_token);
                    var account = accounts.Where(x => x.Id == pageid).FirstOrDefault();
                    if (account != null)
                    {
                        requestUri = string.Format("{0}/{1}/feed?access_token={2}", GRAPH_URL, account.Id, account.AccessToken);
                        //set privacy to null as not supported for page posts
                        privacy = null;
                    }
                }
                //build list of content keys to form post content
                var contentkeys = new List<KeyValuePair<string, string>>();
                //add message
                contentkeys.Add(new KeyValuePair<string, string>("message", message));
                //add link if exists
                if (!string.IsNullOrWhiteSpace(link))
                {
                    contentkeys.Add(new KeyValuePair<string, string>("link", link));
                }
                //append photo ids
                for (int i = 0; i < photoids.Count; i++)
                {
                    var key = string.Format("attached_media[{0}]", i);
                    var value = string.Format("{{\"media_fbid\":\"{0}\"}}", photoids[i]);
                    contentkeys.Add(new KeyValuePair<string, string>(key, value));
                }
                //append privacy information (user posts only)
                if (!string.IsNullOrWhiteSpace(privacy))
                {
                    var privacycontent = string.Format("{{\"value\": \"{0}\"}}", privacy);
                    contentkeys.Add(new KeyValuePair<string, string>("privacy", privacycontent));
                }
                //append scheduled time information
                if (scheduledtime.HasValue)
                {
                    //get unix timestamp
                    var unixtimestamp = Convert.ToInt32((scheduledtime.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
                    //set published to false for a scheduled post
                    contentkeys.Add(new KeyValuePair<string, string>("published", false.ToString()));
                    //append scheduled time in unix timestamp format
                    contentkeys.Add(new KeyValuePair<string, string>("scheduled_publish_time", unixtimestamp.ToString()));
                }
                //build formcontent from content keys collection
                var formContent = new FormUrlEncodedContent(contentkeys);
                //post message
                var response = await _Client.PostAsync(requestUri, formContent);
                //retrieve post identifier if post successful
                if (response.IsSuccessStatusCode)
                {
                    //read json string
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                    postid = d.id;
                }
            }
            return postid;
        }
        /// <summary>
        /// Posts photo to facebook using the url method
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <param name="pageid">Page Identifier</param>
        /// <param name="url">Url of Image</param>
        /// <param name="caption">Photo Caption</param>
        /// <param name="privacy">Privacy Option for Post</param>
        /// <param name="published">Published status for photo</param>
        /// <returns>Photo Id string</returns>
        public static async Task<string> PostPhotoUrl(string user_access_token, string pageid, string url, string caption, string privacy, bool published)
        {
            string photoid = null;
            if (!string.IsNullOrWhiteSpace(user_access_token) && !string.IsNullOrWhiteSpace(url))
            {
                //build request uri based on whether the post is to a page or a user
                var requestUri = string.Format("{0}/me/photos?access_token={1}", GRAPH_URL, user_access_token);
                if (!string.IsNullOrWhiteSpace(pageid))
                {
                    var accounts = await LoadAccounts(user_access_token);
                    var account = accounts.Where(x => x.Id == pageid).FirstOrDefault();
                    if (account != null)
                    {
                        requestUri = string.Format("{0}/{1}/photos?access_token={2}", GRAPH_URL, account.Id, account.AccessToken);
                        //set privacy to null as it is not supported for page posts
                        privacy = null;
                    }
                }
                //build list of content keys to form post content
                var contentkeys = new List<KeyValuePair<string, string>>();
                //add url
                contentkeys.Add(new KeyValuePair<string, string>("url", url));
                //add caption if exists
                if (!string.IsNullOrWhiteSpace(caption))
                {
                    contentkeys.Add(new KeyValuePair<string, string>("caption", caption));
                }
                //add privacy if exists
                if (!string.IsNullOrWhiteSpace(privacy))
                {
                    var privacycontent = string.Format("{{\"value\": \"{0}\"}}", privacy);
                    contentkeys.Add(new KeyValuePair<string, string>("privacy", privacycontent));
                }
                //add published status
                contentkeys.Add(new KeyValuePair<string, string>("published", published.ToString()));
                //build form content from content keys
                var formContent = new FormUrlEncodedContent(contentkeys);
                //post photo to facebook
                var response = await _Client.PostAsync(requestUri, formContent);
                //retriev photo identifier if post successful
                if (response.IsSuccessStatusCode)
                {
                    //read json string
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                    photoid = d.id;
                }
            }
            return photoid;
        }
        /// <summary>
        /// Posts photo to facebook using the data method
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <param name="pageid">Page Identifier</param>
        /// <param name="photo">Photo in IFormFile format from upload</param>
        /// <param name="caption">Photo Caption</param>
        /// <param name="privacy">Privacy Option for Post</param>
        /// <param name="published">Published status for photo</param>
        /// <returns>Photo Id string</returns>
        public static async Task<string> PostPhotoData(string user_access_token, string pageid, IFormFile photo, string caption, string privacy, bool published)
        {
            string photoid = null;
            if (!string.IsNullOrWhiteSpace(user_access_token))
            {
                //build request uri based on whether the post is to a page or a user
                var requestUri = string.Format("{0}/me/photos?access_token={1}", GRAPH_URL, user_access_token);
                if (!string.IsNullOrWhiteSpace(pageid))
                {
                    var accounts = await LoadAccounts(user_access_token);
                    var account = accounts.Where(x => x.Id == pageid).FirstOrDefault();
                    if (account != null)
                    {
                        requestUri = string.Format("{0}/{1}/photos?access_token={2}", GRAPH_URL, account.Id, account.AccessToken);
                        //set privacy to null as it is not supported for page posts
                        privacy = null;
                    }
                }
                //build photo content for post
                var rs = photo.OpenReadStream();
                var photocontent = new StreamContent(rs);
                photocontent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = photo.Name,
                    FileName = photo.FileName
                };
                photocontent.Headers.ContentType = new MediaTypeHeaderValue(photo.ContentType);
                //build remainder of posts using keys
                var contentkeys = new List<KeyValuePair<string, string>>();
                //add caption if exists
                if (!string.IsNullOrWhiteSpace(caption))
                {
                    contentkeys.Add(new KeyValuePair<string, string>("caption", caption));
                }
                //add privacy if exists
                if (!string.IsNullOrWhiteSpace(privacy))
                {
                    var privacycontent = string.Format("{{\"value\": \"{0}\"}}", privacy);
                    contentkeys.Add(new KeyValuePair<string, string>("privacy", privacycontent));
                }
                //add published status
                contentkeys.Add(new KeyValuePair<string, string>("published", published.ToString()));

                //build form content from keys
                var formcontent = new FormUrlEncodedContent(contentkeys);
                //build multi form data content from photo content and other post content
                var multiContent = new MultipartFormDataContent(){
                    photocontent,
                    formcontent
                };

                //post photo to Facebook
                var response = await _Client.PostAsync(requestUri, multiContent);
                //retrieve photo identifier if post successful
                if (response.IsSuccessStatusCode)
                {
                    //read json string
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                    photoid = d.id;
                }
                else
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    //deserialize json
                    dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                }

            }
            return photoid;
        }

        /// <summary>
        /// Retrieves details about a post from Facebook
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <param name="postid">Post Identifier</param>
        /// <returns>Post Details</returns>
        public static async Task<Models.FacebookDetailPost> GetPostDetail(string user_access_token, string postid)
        {
            Models.FacebookDetailPost result = null;
            //buld detail request, including fields
            var requestUri = string.Format("{0}/{1}?access_token={2}&fields=id,created_time,updated_time,story,message,from,is_published,privacy,shares,permalink_url,comments,reactions,sharedposts{{id,created_time,from,message}}", GRAPH_URL, postid, user_access_token);
            //request detail from Facebook
            var response = await _Client.GetAsync(requestUri);
            //proceed with processing of data if request successful
            if (response.IsSuccessStatusCode)
            {
                //read json string
                var jsonstring = response.Content.ReadAsStringAsync();
                jsonstring.Wait();
                //deserialize json
                dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                //populate result object
                result = new Models.FacebookDetailPost();
                result.Id = d.id;
                result.CreatedTime = d.created_time;
                result.UpdatedTime = d.updated_time;
                result.Story = d.story;
                result.Message = d.message;
                result.FromName = d.from.name;
                result.IsPublished = d.is_published;
                result.PrivacyDescription = d.privacy.description;
                result.ShareCount = d.shares != null ? d.shares.count : null;
                result.PermalinkUrl = d.permalink_url;
                if (d.comments != null)
                {
                    if (d.comments.data != null)
                    {
                        foreach (var tempcomment in d.comments.data)
                        {
                            var comment = new Models.FacebookDetailComment();
                            comment.Id = tempcomment.id;
                            comment.CreatedTime = tempcomment.created_time;
                            comment.FromName = tempcomment.from.name;
                            comment.Message = tempcomment.message;
                            result.Comments.Add(comment);
                        }
                    }
                }
                if (d.reactions != null)
                {
                    if (d.reactions.data != null)
                    {
                        foreach (var tempreaction in d.reactions.data)
                        {
                            var reaction = new Models.FacebookDetailReaction();
                            reaction.Id = tempreaction.id;
                            reaction.Name = tempreaction.name;
                            reaction.Type = tempreaction.type;
                            result.Reactions.Add(reaction);
                        }
                    }
                }
                if (d.sharedposts != null)
                {
                    if (d.sharedposts.data != null)
                    {
                        foreach (var tempsharedpost in d.sharedposts.data)
                        {
                            var share = new Models.FacebookDetailShare();
                            share.Id = tempsharedpost.id;
                            share.CreatedTime = tempsharedpost.created_time;
                            share.FromName = tempsharedpost.from != null ? tempsharedpost.from.name : "";
                            share.Message = tempsharedpost.message;
                            result.Shares.Add(share);
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Retrieves details about a post's photo from Facebook
        /// </summary>
        /// <param name="user_access_token">User Access Token</param>
        /// <param name="photoid">Photo Identifier</param>
        /// <returns>Photo Details</returns>
        public static async Task<Models.FacebookDetailPostPhoto> GetPostPhotoDetail(string user_access_token, string photoid)
        {
            string token = user_access_token;
            Models.FacebookDetailPostPhoto result = null;
            //buld detail request, including fields
            var requestUri = string.Format("{0}/{1}?access_token={2}&fields=id,created_time,updated_time,album,from,name,width,height,link,comments,reactions,sharedposts{{id,created_time,from,message}}", GRAPH_URL, photoid, token);
            //request detail from Facebook
            var response = await _Client.GetAsync(requestUri);
            //proceed with processing of data if request successful
            if (response.IsSuccessStatusCode)
            {
                //read json string
                var jsonstring = response.Content.ReadAsStringAsync();
                jsonstring.Wait();
                //deserialize json
                dynamic d = JsonConvert.DeserializeObject(jsonstring.Result);
                //populate result object
                result = new Models.FacebookDetailPostPhoto();
                result.Id = d.id;
                result.CreatedTime = d.created_time;
                result.UpdatedTime = d.updated_time;
                result.AlbumName = d.album.name;
                result.FromName = d.from.name;
                result.Name = d.name;
                result.Width = d.width;
                result.Height = d.height;
                result.Link = d.link;
                if (d.comments != null)
                {
                    if (d.comments.data != null)
                    {
                        foreach (var tempcomment in d.comments.data)
                        {
                            var comment = new Models.FacebookDetailComment();
                            comment.Id = tempcomment.id;
                            comment.CreatedTime = tempcomment.created_time;
                            comment.FromName = tempcomment.from.name;
                            comment.Message = tempcomment.message;
                            result.Comments.Add(comment);
                        }
                    }
                }
                if (d.reactions != null)
                {
                    if (d.reactions.data != null)
                    {
                        foreach (var tempreaction in d.reactions.data)
                        {
                            var reaction = new Models.FacebookDetailReaction();
                            reaction.Id = tempreaction.id;
                            reaction.Name = tempreaction.name;
                            reaction.Type = tempreaction.type;
                            result.Reactions.Add(reaction);
                        }
                    }
                }
                if (d.sharedposts != null)
                {
                    if (d.sharedposts.data != null)
                    {
                        foreach (var tempsharedpost in d.sharedposts.data)
                        {
                            var share = new Models.FacebookDetailShare();
                            share.Id = tempsharedpost.id;
                            share.CreatedTime = tempsharedpost.created_time;
                            share.FromName = tempsharedpost.from != null ? tempsharedpost.from.name : "";
                            share.Message = tempsharedpost.message;
                            result.Shares.Add(share);
                        }
                    }
                }
            }
            return result;
        }
    }
}