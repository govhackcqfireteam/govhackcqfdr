﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GovHackCQFireTeam.Models
{
    public class BOMForecast
    {
        public string Location { get; set; }
        public DateTime Timestamp { get; set; }
        public int TimeStampYear
        {
            get
            {
                return Timestamp.Year;
            }
            set { }
        }
        public int TimeStampMonth
        {
            get
            {
                return Timestamp.Month;
            }
            set { }
        }
        public int TimeStampDay
        {
            get
            {
                return Timestamp.Day;
            }
            set { }
        }
        //[JsonPropertyAttribute(DefaultValueHandling = DefaultValueHandling.Include)]
        public int TimeStampHour
        {
            get
            {
                return Timestamp.Hour;
            }
            set { }
        }

        public int TimeStampMinute
        {
            get
            {
                return Timestamp.Minute;
            }
            set { }
        }

        public DateTime UpdatedAt { get; set; }
        public double? TemperatureAirC { get; set; }
        public double? TemperatureDewPointC { get; set; }
        public double? HumidityRelativePercent { get; set; }
        public double? WindSpeedKmH { get; set; }
        public string WindDirection { get; set; }
        public double? ForestFuelDrynessFactor { get; set; }
        public double? UserWindReductionFactor { get; set; }
        public double? UserFuelLoad { get; set; }
        public double? UserSlope { get; set; }

        public double? FFDI
        {
            get
            {
                if (TemperatureAirC.HasValue && ForestFuelDrynessFactor.HasValue && HumidityRelativePercent.HasValue && WindSpeedKmH.HasValue && UserWindReductionFactor.HasValue)
                {
                    if (TemperatureAirC.Value == 0)
                        return 0;
                    else
                        return 2 * Math.Exp(-0.45 + 0.987 * Math.Log(ForestFuelDrynessFactor.Value) - 0.0345 * HumidityRelativePercent.Value + 0.0338 * TemperatureAirC.Value + 0.0234 * WindSpeedKmH.Value * 3 / UserWindReductionFactor.Value);
                }
                return null;
            }
            set { }
        }

        
        public double? FROSMpH
        {
            get
            {
                if (FFDI.HasValue && UserFuelLoad.HasValue && UserSlope.HasValue)
                {
                    return 0.0012 * FFDI.Value * UserFuelLoad.Value * Math.Exp(0.063 * UserSlope.Value) * 1000;
                }
                return null;
            }
            set { }
        }

        
        public BOMForecast()
        {
            UpdatedAt = DateTime.Now;
            UserWindReductionFactor = 3;
            UserFuelLoad = 10;
            UserSlope = 10;
        }
    }
}