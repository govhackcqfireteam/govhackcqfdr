﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GovHackCQFireTeam.Models
{
    public class BOMLocation
    {
        public string id { get; set; }
        public string type { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string geohash { get; set; }
    }
}
