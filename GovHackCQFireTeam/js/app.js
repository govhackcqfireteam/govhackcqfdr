﻿$(function () {
    if (FireApp == null) {
        FireApp = new FireAppObject();
    }
});

zeroPad = function (number) {
    return ('0' + number).slice(-2);
};

function FireAppObject() {
    this.server = 'api';
    this.days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    this.selected = null;
    this.locations = {};
}

FireAppObject.prototype.processForecast = function (location) {   
    var self = this;
    
    var retFunc = function (data) {
        var dayData = self.processData(data,location);
        overview.createPages(dayData,location);
    }
    

    if (location.geohash == null || location.state == null) {
        self.BOMForecastLoadLocation(location, retFunc);
    } else {
        self.BOMForecastLoadGrid(location.geohash, location.state, location.name, retFunc);
    }
    
}

FireAppObject.prototype.processData = function (data,location)
{
    var self = this;
    var returnData = {};
    for (var dataKey in data)
    {
        var item = data[dataKey];
        if (item['FFDI'] != null) {
            item['FFDI'] = (Math.round(item['FFDI']) / 10) * 10;
        }
        if (item['FROSMpH'] != null) {
            item['FROSMpH'] = (Math.round(item['FROSMpH']) / 10) * 10;
        }
        if (item['FFDI'] != null) {
            item['FDR'] = self.getFireDangerLevel(item['FFDI']).name;
        } else {
            item['FDR'] = null;
        }
        
        
        var day = [];
        var dayKey = item['TimeStampYear'] + "-" + item['TimeStampMonth'] + "-" + item['TimeStampDay'];
        var timeKey = zeroPad(item['TimeStampHour']) + ":" + zeroPad(item['TimeStampMinute']);
        if (returnData[dayKey] == null) {
            returnData[dayKey] = {};
            returnData[dayKey]['times'] = {};
            returnData[dayKey]['raw'] =[];
            returnData[dayKey]['max'] = {};
        }
        if (returnData[dayKey]['times'][timeKey] == null) {
            returnData[dayKey]['times'][timeKey] = item;
        }
        returnData[dayKey]['raw'].push(item);
    }

    for (var i in returnData) {
        var raws = returnData[i]['raw'];
        var day = new Date(i);

        returnData[i]['max'] = self.getMaxTypes(raws);
        returnData[i]['day'] = item['TimeStampDay'] + "/" + item['TimeStampMonth'] + "/" + item['TimeStampYear'];
        returnData[i]['dayTime'] = day;
        returnData[i]['dayNumber'] = day.getDay();
        returnData[i]['location'] = location        
    }
    return returnData;
}

FireAppObject.prototype.deteremineColor = function (field, value, object) {
    var self = this;
    var colors = [];
    colors['TemperatureAirC'] = {
        high: 32,
        low: 10
    }

    colors['HumidityRelativePercent'] = {
        low: 3,
        lowClass:'color-low'
    }

    colors['WindSpeedKmH'] = {
        high: 18
    }

    colors['TemperatureDewPointC'] = {
        high: object['TemperatureAirC'],
        highClass: 'color-low'
    }

    if (field == 'FFDI') {
        var dangerClass = "level level-"+self.getFireDangerLevel(value).name.toLowerCase().replace(" ", "");
        return dangerClass;
    }

    if (field == 'FDR') {
        var dangerClass = "level level-" + self.getFireDangerLevel(object['FFDI']).name.toLowerCase().replace(" ", "");
        return dangerClass;
    }

    

    if (colors[field] != null) {
        if (colors[field].high != null) {
            if (value >= colors[field].high) {
                if (colors[field].highClass != null) {
                    return colors[field].highClass;
                }
                return "color-high";
            }
        }

        if (colors[field].low != null) {
            if (value <= colors[field].low) {
                if (colors[field].lowClass != null) {
                    return colors[field].lowClass;
                }
                return "color-low";
            }
        }        
    }
    return null;

}

FireAppObject.prototype.createTable = function (dayData) {

    var day = dayData['times'];
    var self = this;
    
    var tableRows = {
        "Air Temperature": "TemperatureAirC",
        "Relative Humidity (%)": "HumidityRelativePercent",
        "Wind Speed km/h": "WindSpeedKmH",
        "Wind Direction": "WindDirection",
        "Forest Fuel Dryness Factor": "ForestFuelDrynessFactor",
        "FFDI": "FFDI",
        "FDR": "FDR",
        "Fuel Load": "UserFuelLoad",
        "Slope (deg)": "UserSlope",
        "FROS (meters per hour)": "FROSMpH",
        "Dew point temperature": "TemperatureDewPointC"
    }
    
    var rows = {};
    var dailyTableParent = elementHelper.getTemplate(".template-daily-overview");
    dailyTableParent.find(".title").addClass("content-dateFull");


    var table = $("<table>");
    table.addClass("table").addClass("table-responsive");

    var thead = $("<thead>");
    var theadTr = $("<tr>");
    var thBase = $("<th>");
    var dayTH = thBase.clone().html("Time");
    theadTr.append(dayTH);

    for (var timeKey in day) {
        var item = day[timeKey];
        var th = thBase.clone().html(timeKey).addClass("level-header");
        theadTr.append(th);
    }
    thead.append(theadTr);
    table.append(thead);

    tbody = $("<tbody>");



    for (var item in tableRows) {
        var row = $("<tr>");
        var td = $("<td>");
        td.html(item);
        row.append(td);
        for (var timeKey in day) {
            var BOMForecast = day[timeKey];
            var td = $("<td>");
            var content = "NA";

            if (BOMForecast[tableRows[item]] != null) {
                content = BOMForecast[tableRows[item]];
                var colorClass = self.deteremineColor(tableRows[item], content,day[timeKey]);
                if (colorClass != null) {
                    td.addClass(colorClass);
                }
            }
            td.html($("<label>").html(content));
            row.append(td);
        }
        tbody.append(row);
    }
    table.append(tbody);
    return table;
}

FireAppObject.prototype.prepareChartData = function (day)
{
    var data = day['times'];
    var chartData = [];
    var labels = [];
    for (var i in data) {
        var item = data[i];
        var amount = data[i]['FFDI'];
        if (amount != null) {
            chartData.push(amount);
            labels.push(i)
        }
    }
    var ret = {};
    ret.chartData = chartData;
    ret.labels = labels;
    return ret;
}

FireAppObject.prototype.getFireDangerLevel = function (amount)
{
    var self = this;
    var dangers = self.getFireDangers();
    for (var i in dangers)
    {
        if (amount < i)
        {
            return dangers[i];
        }
    }

    return null;
}

FireAppObject.prototype.getFireDangers = function ()
{
    var lookup = {};
    lookup[5] = {
        name: "Low",
        shortName: "L",
        level: 0,
        max: 5,
        min: 0
    }
    lookup[12] = {
        name: "Moderate",
        shortName: "M",
        level: 1,
        max: 12,
        min: 5

    }
    lookup[32] = {
        name: "High",
        shortName: "H",
        level: 2,
        max: 32,
        min: 12
    }
    lookup[50] = {
        name: "Very High",
        shortName: "VH",
        level: 3,
        max: 50,
        min: 32
    }
    lookup[75] = {
        name: "Severe",
        shortName: "S",
        level: 4,
        max: 75,
        min: 50
    }
    lookup[100] = {
        name: "Extreme",
        shortName: "E",
        level: 5,
        max: 100,
        min: 75
    }
    lookup[9999] = {
        name: "Catastrophic",
        shortName: "C",
        level: 6,
        max: 999,
        min: 100
    }
    return lookup;
}

FireAppObject.prototype.getMaxTypes = function (data,min) {

    var ret = {};
    for (var key in data) {
        var item = data[key];   
        if (ret == null) { ret = {};}
        for (var prop in item) {
            if (ret[prop] == null) {
                ret[prop] = item;
            } else {
                if (min == true) {
                    if (item[prop] < ret[prop][prop]) {
                        ret[prop] = item;
                    }
                } else {
                    if (item[prop] > ret[prop][prop]) {
                        ret[prop] = item;
                    }
                }
            }
        }
    }
    return ret;
}

FireAppObject.prototype.populateList = function (listElement, searchText) {
    var self = this;
    var element = $(listElement);
    element.html("");
    var select = $("<select>");

    var option = $("<option>");

    this.BOMLocationSearch(searchText, function (data) {
        var locations = {};
        var locationsByName = {};

        for (var i in data) {
            var loc = new Location(data[i]);
            locations[i] = loc;
            var tmpOption = option.clone();
            tmpOption.html(loc['name']);
            tmpOption.val(loc['name']);
            locationsByName[loc['name']] = loc;
            element.append(tmpOption);
        }

        self.locations = locationsByName;
        
    });
}



FireAppObject.prototype.callServer = function (controller, action, clientData, callback) {
    var self = this;
    var url = "/" + self.server + "/" + controller + "/" + action;
    for (var i in clientData) {
        if (clientData[i] != null) {
            url = url + "/" + clientData[i];
        }
    }
    $.ajax({
        url: url,
    }).done(function (data, status) {
        if (status == "success") {
            if (callback != null) {
                callback(data, clientData[2]);
            }
        }
    });
}

FireAppObject.prototype.BOMLocationSearch = function (text, callback) {
    this.callServer("BOMLocation","search",[text],callback);
}

FireAppObject.prototype.BOMForecastLoadLocation = function (location, callback) {
    this.callServer("BOMForecast", "LoadLocation",[null,null,location], callback);
}

FireAppObject.prototype.BOMForecastLoadGrid = function (geohash, state, location, callback) {
    this.callServer("BOMForecast", "LoadGrid", [geohash,state,location], callback);
}


var FireApp = null;

function Location(data) {
    for (var i in data) {
        this[i] = data[i];
    }
}
FireAppObject.prototype.setupChart = function (data) {

    var self = this;

    var levels = {
        'extra': 100,
        'vhigh': 50,
        'high': 24,
        'mod': 12,
        'low':5
    };


    var series = [];
    for (var i in levels)
    {
        var s = {
            name: "series-" + i
        };
        s.data = [];
        for (var i2 = 0; i2 >= self.days; i2++) {
            s.data.push(levels[i]);
        }
        series.push(s);
    }

    var line = {
        name: "series-99"
    };
    for (var i = 0; i < self.days; i++) {
        line.data[i] = (Math.random() * 100);
    }

    series["series-99"] = line;

    var chart = new Chartist.Line('.ct-chart', {
      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      // Naming the series with the series object array notation
      series: [series]
}, {
  fullWidth: true,
  axisX: {
    showLabel: true,
    showGrid: false
  },
  axisY: {
    showLabel: true,
    showGrid: false
  },
  // Within the series options you can use the series names
  // to specify configuration that will only be used for the
  // specific series.
  series: {
    'series-low': {
      showArea: true,
      showLine: false,
      showPoint: false
    },
    'series-mod': {
      lineSmooth: Chartist.Interpolation.simple(),
      showArea: true,
      showLine: false,
      showPoint: false
    },
    'series-high': {
      showArea: true,
      showLine: false,
      showPoint: false
    },
    'series-vhigh': {
      showArea: true,
      showLine: false,
      showPoint: false
    },
    'series-extr': {
      showArea: true,
      showLine: false,
      showPoint: false
    },
    'series-99': {
      lineSmooth: Chartist.Interpolation.simple(),
      showArea: false,
      showLine: true,
      showPoint: true
    }
  }
        }, );

}