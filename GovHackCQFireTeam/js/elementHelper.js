﻿var elementHelper = {};

elementHelper.createCard = function () {
    return $(".templates").find(".template-card").clone();
}

elementHelper.getTemplate = function (element) {
    return $(".templates").find(element).clone();
}

