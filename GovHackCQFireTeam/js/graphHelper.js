﻿$(function () {
    $(".ct-chart").on("populateGraph", function (e, data, labels, dLevels) {
        var days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];

        var dangerLevels = [];
        for (var i in dLevels) {
            dangerLevels.push(dLevels[i]);
        }

        var self = $(this);
        new Chartist.Line(this, {
            labels: labels,
            series: [
                data
            ]
        }, {
                low: 0,
                showArea: true,
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        var date = new Date(value);
                        var day = date.getDay();
                        return (index % 8  === 0 || index == 0) ? days[day] : null;
                    }
                }
            });
    });
});

