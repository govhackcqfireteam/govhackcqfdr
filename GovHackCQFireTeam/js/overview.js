﻿var overview = {};



overview.createPages = function (days,location) {
    var parent = $(".days");
    parent.empty();
    for (var dayKey in days) {
        var day = days[dayKey];
        overview.createPage(day);
    }
    //parent.find(".page-day").first().show();
    $(".searchArea").hide();
}

overview.prepareChart = function (dayElement, day) {
    var dayElement = $(".day-" + day['dayNumber']);
    var chartData = FireApp.prepareChartData(day);

    var chartKey = "ct-chart-" + day['dayNumber'];
    var chartElement = $("." + chartKey);
    dayElement.find(".ct-charts").addClass(chartKey);



    if (chartData.chartData.length > 0) {
        new Chartist.Line('.' + chartKey, {
            labels: chartData.labels,
            series: [
                chartData.chartData
            ]
        }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });
    } else {
        chartElement.html($("<h6>").html("No chart data avilable due to lack of forecasted information"));
    }
}

overview.populateSideSection = function (dayElement, day) {
    var maxFFDI = day['max']['FFDI']['FFDI'];
    var level = FireApp.getFireDangerLevel(maxFFDI);
    dayElement.find(".label-fire-ranges").html(level.min + "-" + level.max);
    dayElement.find(".label-fire-rating").html("Fire Index Rating - " + level.name);

    var image = $(".templates").find(".fire-image-" + level.level).clone();
    dayElement.find(".fire-image").append(image);
}

overview.createPage = function dayData(day) {

    var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']; 
    var table = FireApp.createTable(day);


    var dayElement = $(".templates").find(".page-day").clone().addClass("day-" +day['dayNumber']);
    
    var parent = $(".days");

    dayElement.find(".table-container").html("");
    dayElement.find(".table-container").append(table);

    var maxFFDI = day['max']['FFDI']['FFDI'];
    var level = FireApp.getFireDangerLevel(maxFFDI);

    var today = new Date(day['day']);
    var contents = {
        'suburb': day['location']['name'],
        'state': day['location']['state'],
        'dateFull': dayNames[day['dayNumber']]+" " + day['day'],
        'maxTemp': day['max']['TemperatureAirC']['TemperatureAirC'],
        'maxWind': day['max']['WindSpeedKmH']['WindSpeedKmH'],
        'maxFDI': day['max']['FFDI']['FFDI'],
        'maxFDR': level.name,
        'dayName': dayNames[day['dayNumber']]
    };

    for (var i in contents) {
        var value = contents[i];
        dayElement.find(".content-" + i).html(value);
    }

    overview.populateSideSection(dayElement, day);
    
    //dayElement.hide();
    parent.append(dayElement);
    overview.prepareChart(dayElement, day);

    $(".return-to-search").on("click", function () {
        $(".searchArea").show();
        $(".days").hide();
    });


    


}