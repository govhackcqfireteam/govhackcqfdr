﻿$(function () {

    $("#locationSearch").focus();
    $("#locationSearch").on("change", function () {
        var self = $(this);
        var text = self.val();

        var resultsElement = $("#searchResults");

        resultsElement.html("");
        resultsElement.hide();

        FireApp.BOMLocationSearch(text, function (data) {
            resultsElement.show();         

            var locations = {};
            var locationsByName = {};

            for (var i in data) {
                var loc = new Location(data[i]);
                locations[i] = loc;
                var card = elementHelper.getTemplate(".template-card-small");
                card.find(".card").addClass("card-button").addClass("location-card").data("location",loc);
                card.find(".header").html(loc.name);
                card.find(".description").html(loc.state);
                
                card.hide();
                resultsElement.append(card);
                card.fadeIn("slow");
            }
            self.data("locations", locations);

            $(".location-card").on("click", function () {
                var self = $(this);
                var location = self.data("location");
                var resultsElement = $(".searchArea");
                resultsElement.hide();
                FireApp.processForecast(location);
            });
        });
    });

    
    $("#locationSearch").focus();

});